terraform {
  required_providers {
    tfe = {
      version = "~> 0.45.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "16.0.3"
    }
  }
}
provider "aws" {
  region = "ap-northeast-2"
}

provider "gitlab" {
  //base_url = "https://git.ddim.com/api/v4/"
  token = var.gitlab_token
}

# Bring csv for get colume value to 
data "gitlab_repository_file" "get_csv" {
  project   = "46650333"
  ref       = "main"
  file_path = "${var.index}.csv"
}

locals {
  vm_data = csvdecode(base64decode(data.gitlab_repository_file.get_csv.content))
  vm_running = { for k, v in local.vm_data : v.Name => v if upper(v.Status) == "RUNNING"}
}

resource "aws_instance" "test" {
  for_each   = { for k, v in local.vm_running : k => v }
  ami             = each.value.Image 
  instance_type = each.value.vm_type
  subnet_id = each.value.subnet_id
  private_ip = "${each.value.IP}"
  
  cpu_option {
    core_count = each.value.core
    threads_per_core = each.value.HT
  }

  key_name   = "JIN"
  
  vpc_security_group_ids = split(",", "${each.value.security_group_id}")

  tags = {
    Name = "${each.key}"
    hyc = "STG-EC2"
  }

  lifecycle {
    ignore_changes = [ ami ]
  }
 
 }

 /*
vm 생성이 끝난 후 output으로 결과물을 root로 전달 그리고 root 모듈을 한 번더 실행시켜서 csv를 완전히 완성시켜야함
 */

# resource "aws_network_interface" "network_specification" {
#   for_each   = { for k, v in local.vm_running : k => v }
#   subnet_id   = each.value.subnet_id
#   private_ips = ["${each.value.IP}"]

#   tags = {
#     Name = "${each.key}-primary_network_interface"
#     hyc="STG-ec2"
#   }
# }



// not used resource because This module use only csv which is made by root module

# data "terraform_remote_state" "get_csv" {
#   backend = "remote"
#   config = {
#     organization = "ddimtech-samsung-poc"
#     workspaces = {
#       name = "terraform-instances-csv"
#     }
#   }
# }

# data "http" "example" {
#   url = "https://gitlab.com/api/v4/projects/46650333/repository/files/${var.index}.csv?ref=main"
#   # Optional request headers
#   request_headers = {
#     PRIVATE-TOKEN = var.gitlab_token
#   }
# }



# resource "gitlab_repository_file" "csv" {
#   project   = 46650333
#   file_path = "${var.index}.csv"
#   branch    = "main"
#   // content will be auto base64 encoded
#   content = base64encode("${join(",", local.csv_header)}\n${local.join_test}")
#   //author_email   = "terraform@example.com"
#   //author_name    = "Terraform"
#   commit_message      = "test2"
#   overwrite_on_create = true
# }
# output "csv_response" { value = local.csv_response }
# output "vm_naming" { value = local.vm_naming }
# output "test_map" { value = local.test_map }
# output "join_test" { value = local.join_test }

